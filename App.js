import React from 'react';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import reducers from './src/reducers';
import {StatusBar} from 'react-native';
import AppContainer from './src/routes';
import splashScreen from './assets/splash.png';
import {createStore, applyMiddleware} from 'redux';
import * as SplashScreen from 'expo-splash-screen';
import AnimatedSplashLoader from './src/components/AnimatedSplashScreen';

SplashScreen.preventAutoHideAsync().catch(() => {
	/* Reloading the app might trigger some race conditions, ignore them */
});

export default function App() {
	return (
		<Provider store={createStore(reducers, applyMiddleware(thunk))}>
			<AnimatedSplashLoader image={splashScreen}>
				<StatusBar barStyle="light-content" />
				<AppContainer />
			</AnimatedSplashLoader>
		</Provider>
	);
}

import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import TrailerList from '../../components/TraiilerList';

const UpcomingDetailView = ({route}) => (
	<View >
		<TrailerList id={route.params.id} />
	</View>
);

UpcomingDetailView.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.number.isRequired,
		}).isRequired,
	}).isRequired,
};

export default UpcomingDetailView;

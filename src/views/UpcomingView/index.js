import React from 'react';
import styles from './styles';
import {View} from 'react-native';
import UpcomingMovieList from '../../components/UpcomingMovieList';

const UpcomingView = () => (
	<View style={styles.container}>
		<UpcomingMovieList />
	</View>
);

export default UpcomingView;

import React from 'react';
import styles from './styles';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import MovieList from '../../components/MovieList';

const CinemasDetailView = ({route}) => (
	<View style={styles.container}>
		<MovieList id={route.params.id}/>
	</View>
);

CinemasDetailView.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			id: PropTypes.number.isRequired,
		}).isRequired,
	}).isRequired,
};

export default CinemasDetailView;

import React from 'react';
import styles from './styles';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import MovieDetailList from '../../components/MovieDetailList';

const MovieDetailView = ({route}) => (
	<View style={styles.container}>
		<MovieDetailList
			movieId={route.params.movieId}
			cinemaId={route.params.cinemaId} />
	</View>
);

MovieDetailView.propTypes = {
	route: PropTypes.shape({
		params: PropTypes.shape({
			movieId: PropTypes.number.isRequired,
			cinemaId: PropTypes.number.isRequired,
		}).isRequired,
	}).isRequired,
};

export default MovieDetailView;

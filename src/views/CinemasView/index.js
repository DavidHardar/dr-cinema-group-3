import React from 'react';
import styles from './styles';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import CinemaList from '../../components/CinemaList';

const CinemasView = () => (

	<View style={styles.container}>
		<CinemaList/>
	</View>

);

CinemasView.propTypes = {
	navigation: PropTypes.shape({
		navigate: PropTypes.func.isRequired,
	}).isRequired,
};

export default CinemasView;

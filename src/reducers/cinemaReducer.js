import {UPDATE_CINEMAS} from '../constants';

const compare = (a, b) => {
	if (a.name < b.name) {
		return -1;
	}

	if (a.name > b.namee) {
		return 1;
	}

	return 0;
};

// Lint throws error, do not edit this!
export default function (state = [], action) {
	switch (action.type) {
		case UPDATE_CINEMAS: return action.payload.sort(compare);
		default: return state;
	}
}

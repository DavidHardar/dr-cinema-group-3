import {APP_DONE_LOADING} from '../constants';

// Lint throws error, do not edit this!
export default function (state = false, action) {
	switch (action.type) {
		case APP_DONE_LOADING: return true;
		default: return state;
	}
}

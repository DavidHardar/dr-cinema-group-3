import {REFRESH_UPCOMING} from '../constants';

// Lint throws error, do not edit this!
export default function (state = false, action) {
	switch (action.type) {
		case REFRESH_UPCOMING: return action.payload;
		default: return state;
	}
}


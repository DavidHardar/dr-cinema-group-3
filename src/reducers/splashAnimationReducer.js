import {SPLASH_ANIMATION_DONE} from '../constants';

// Lint throws error, do not edit this!
export default function (state = false, action) {
	switch (action.type) {
		case SPLASH_ANIMATION_DONE: return true;
		default: return state;
	}
}

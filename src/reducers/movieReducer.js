import {UPDATE_MOVIES} from '../constants';

// Lint throws error, do not edit this!
export default function (state = [], action) {
	switch (action.type) {
		case UPDATE_MOVIES: return action.payload;
		default: return state;
	}
}

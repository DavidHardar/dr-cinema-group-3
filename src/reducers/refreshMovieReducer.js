import {REFRESH_MOVIE} from '../constants';

// Lint throws error, do not edit this!
export default function (state = false, action) {
	switch (action.type) {
		case REFRESH_MOVIE: return action.payload;
		default: return state;
	}
}

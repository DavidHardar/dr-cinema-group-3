import {REFRESH_CINEMA} from '../constants';

// Lint throws error, do not edit this!
export default function (state = false, action) {
	switch (action.type) {
		case REFRESH_CINEMA: return action.payload;
		default: return state;
	}
}

import {NEXT_VIDEO, PREV_VIDEO, RESET_INDEX} from '../constants';

const previousVideo = (state, limit) => {
	let newState = -1;
	// If at first video loop to last
	if (state > 0) {
		newState = state - 1;
	} else {
		newState = limit;
	}

	return newState;
};

const nextVideo = (state, limit) => {
	let newState = -1;
	// If at last video loop to first
	if (state < limit) {
		newState = state + 1;
	} else {
		newState = 0;
	}

	return newState;
};

// Lint throws error, do not edit this!
export default function (state = 0, action) {
	switch (action.type) {
		case PREV_VIDEO: return previousVideo(state, action.payload);
		case NEXT_VIDEO: return nextVideo(state, action.payload);
		case RESET_INDEX: return 0;
		default: return state;
	}
}

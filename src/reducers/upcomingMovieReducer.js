import {UPDATE_UPCOMING_MOVIES} from '../constants';
import moment from 'moment';

const compare = (a, b) => {
	const aDate = moment(a['release-dateIS']);
	const bDate = moment(b['release-dateIS']);
	if (aDate < bDate) {
		return -1;
	}

	if (aDate > bDate) {
		return 1;
	}

	return 0;
};

// Lint throws error, do not edit this!
export default function (state = [], action) {
	switch (action.type) {
		case UPDATE_UPCOMING_MOVIES: return action.payload.sort(compare);
		default: return state;
	}
}

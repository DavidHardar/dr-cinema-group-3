import movies from './movieReducer';
import cinemas from './cinemaReducer';
import {combineReducers} from 'redux';
import appLoading from './appLoadingReducer';
import movieRefresh from './refreshMovieReducer';
import cinemaRefresh from './refreshCinemaReducer';
import ytIndex from './youtubeVideoRotationReducer';
import upcomingMovies from './upcomingMovieReducer';
import upcomingRefresh from './refreshUpcomingReducer';
import splashAnimation from './splashAnimationReducer';
import cinemaDetailsRefresh from './refreshCinemaDetailsReducer';
import upcomingDetailsRefresh from './refreshUpcomingDetailsReducer';

export default combineReducers({
	movies,
	cinemas,
	ytIndex,
	appLoading,
	movieRefresh,
	cinemaRefresh,
	upcomingMovies,
	splashAnimation,
	upcomingRefresh,
	cinemaDetailsRefresh,
	upcomingDetailsRefresh,
});

import * as apiService from './apiService';
import * as cleanerService from './cleanerService';

export const cinemaFilter = async () => {
	const cinemas = await apiService.getCinemas();
	// Takes away the google_map key, value pair. Filters unnesasary data
	const cinemasRet = await cinemas.map(({google_map, ...rest}) => ({...rest}));
	const cinemaClean = await cleanerService.cinemaCleaner(cinemasRet);
	return cinemaClean;
};

export const movieFilter = async () => {
	const movies = await apiService.getMovies();
	// Takes all unnesasary data from the movie array, open for extention
	const moviesRet = await movies.map(({_id, actors_abridged, alternativeTitles, certificate, certificateIS, directors_abridged, ids, omdb, ratings, rotten_critics, ...rest}) => (
		{...rest}));
	const movieClean = await cleanerService.movieCleaner(moviesRet);
	return movieClean;
};

export const upcomingMovieFilter = async () => {
	const upcomingMovies = await apiService.getUpcomingMovies();
	// Same as movieFilter, removes unnesasary data that the API provides
	const upcomingMoviesRet = await upcomingMovies.map(({actors_abridged, alternativeTitles, directors_abridged, ids, omdb, _id, ...rest}) => (
		{...rest}));
	const upcomingMovieClean = await cleanerService.movieCleaner(upcomingMoviesRet);
	return upcomingMovieClean;
};


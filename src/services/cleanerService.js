import {NO_CINEMA_DESCRIPTION} from '../constants';
import {logoArray, genericLogo} from '../resources/data';
import {genreList} from './apiService';

export const cinemaCleaner = cinemaArr => {
	if (cinemaArr.length !== 0) {
		for (let i = 0; i < cinemaArr.length; i++) {
			// Adding a phone number to Bio Paradis
			if (cinemaArr[i].id === 5) {
				cinemaArr[i].phone = '412-7711';
			}

			// Here we add a new key, value pair of logos for cinemas
			if (i <= logoArray.length) {
				cinemaArr[i].logo = logoArray[cinemaArr[i].id];
			} else { // If there is a new cinema, we add the Dr.Cinema logo
				cinemaArr[i].logo = genericLogo;
			}

			// Changing the weird key with spaces to an address key with no spaces
			cinemaArr[i].address = cinemaArr[i]['address	'];
			delete cinemaArr[i]['address	'];

			// Filter html tags in the description
			if (cinemaArr[i].description !== null && cinemaArr[i].description !== undefined && cinemaArr[i].description !== '') {
				const newDescr = cinemaArr[i].description.replace(/(<([^>]+)>)/gi, '');
				cinemaArr[i].description = newDescr;
			} else {// If no description is available we display a string that explains the cinema has no description
				cinemaArr[i].description = NO_CINEMA_DESCRIPTION;
			}
		}
	}

	return cinemaArr;
};

export const movieCleaner = movieArr => {
	for (let i = 0; i < movieArr.length; i++) {
		// Here we find movies that have an array of genre ids, and not an array of genres
		if (!isNaN(movieArr[i].genres[0])) {
			// Go over the genre ids for the given movie
			for (let j = 0; j < movieArr[i].genres.length; j++) {
				const genreId = movieArr[i].genres[j];

				for (let k = 0; k < genreList.length; k++) {
					// Find the genre applicable to our genre id
					if (genreId === genreList[k].ID) {
						movieArr[i].genres[j] = genreList[k];
					}
				}
			}
		}

		if (movieArr[i].plot === null || movieArr[i].plot === undefined || movieArr[i].plot === '') {
			movieArr[i].plot = NO_CINEMA_DESCRIPTION;
		}

		// If movie has no poster we give them a default Dr.Cinema poster
		if (movieArr[i].poster === 'https://kvikmyndir.is/images/poster/') {
			movieArr[i].poster = 'https://i.ibb.co/njDH3GP/splash.png';
		}

		// All movies with no trailers are given the value for trailers as null
		if (movieArr[i].trailers.length === 0) {
			movieArr[i].trailers = null;
		} else if (movieArr[i].trailers[0].results.length === 0) {
			movieArr[i].trailers = null;
		}
	}

	return movieArr;
};

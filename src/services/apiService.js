const axios = require('axios');

const api = 'https://api.kvikmyndir.is';
export let token = '';
export let genreList = [];
// Get auth token
export const authentication = async () => {
	const promise = axios.post(api.concat('/authenticate'), {
		username: 'hordurh20',
		password: 'App-3-Group-3!~',
	});
	const dataPromise = await promise;

	token = dataPromise.data.token;
	return dataPromise.data.token;
};

// Make a request for all cinemas
export const getCinemas = async () => {
	const theaters = await axios.get(api.concat('/theaters'), {
		headers: {
			'x-access-token': token,
		},
	});
	return theaters.data;
};

// Make a request for all movies
export const getMovies = async () => {
	const movies = await axios.get(api.concat('/movies'), {
		headers: {
			'x-access-token': token,
		},
	});
	return movies.data;
};

// Make a request for all upcoming movies
export const getUpcomingMovies = async () => {
	const upcomingMovies = await axios.get(api.concat('/upcoming'), {
		headers: {
			'x-access-token': token,
		},
	});
	return upcomingMovies.data;
};

// Make a request for all genres
export const getGenres = async () => {
	const genres = await axios.get(api.concat('/genres'), {
		headers: {
			'x-access-token': token,
		},
	});
	genreList.push(genres.data);
	genreList = genreList[0];
	return genres.data;
};

export const logoArray = [
	'',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_smarabio.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_haskolabio.png',

	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_borgarbio.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_laugarasbio.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_bioparadis.png',

	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_sambioalfabakka.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_sambiokringlunni.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_sambioegilsholl.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_sambioakureyri.png',
	'https://sambio.blob.core.windows.net/files/sambio.png',
	'https://kvikmyndir.is/wp-content/themes/kvikmyndir/images/logo_sambiokeflavik.png',

];

export const genericLogo = 'https://lh3.googleusercontent.com/proxy/YfnX6QSE2yHWCa9PO7lK0IKmV8nwKKF1fX7TaIDS_39KHLoF7kAbUcXcJtkcBG4oRSrIo97471Cjh0K282uWuBAO8UAcBXqus0plUPx01XGuZ72l5Y-Tls5biHwGQbjC8AHw8Sq10jBlryeA_vHWzA';

import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import Movie from '../../components/Movie';
import MovieListEmpty from '../MovieListEmpty';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies} from '../../actions/getMoviesAction';
import CinemaDetail from '../../components/CinemaDetail';
import {View, RefreshControl, FlatList} from 'react-native';
import {setCinemaDetailsRefresh} from '../../actions/refreshCinemaDetailsAction';

const MovieList = ({id}) => {
	const cinemaDetailsRefresh = useSelector(state => state.cinemaDetailsRefresh);
	const movies = useSelector(state => state.movies);
	const dispatch = useDispatch();

	const onRefresh = React.useCallback(async () => {
		dispatch(setCinemaDetailsRefresh(true));
		dispatch(getMovies());
		dispatch(setCinemaDetailsRefresh(false));
	}, [cinemaDetailsRefresh]);

	const movieListForCinema = [];
	// Iterates over all movies and finds the movies corresponding to the appropriate cinema
	for (let i = 0; i < movies.length; i++) {
		const singleMovie = movies[i];
		for (let j = 0; j < singleMovie.showtimes.length; j++) {
			const showtime = singleMovie.showtimes[j];
			if (showtime.cinema.id === id) {
				movieListForCinema.push(movies[i]);
			}
		}
	}

	return (
		<View style={styles.container}>
			<FlatList
				data={movieListForCinema}
				ListEmptyComponent={<MovieListEmpty/>}
				refreshControl={
					<RefreshControl
						refreshing={cinemaDetailsRefresh}
						onRefresh={onRefresh} />
				}
				renderItem={({item}) => (
					<Movie
						movieId={item.id}
						cinemaId={id}
					/>
				)}
				ListHeaderComponent= {<CinemaDetail id = {id}/>}
				keyExtractor={movieListForCinema => movieListForCinema.id} />

		</View>
	);
};

MovieList.propTypes = {
	id: PropTypes.number.isRequired,
};

export default MovieList;

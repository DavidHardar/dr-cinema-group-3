import React from 'react';
import styles from './styles';
import {View, Text} from 'react-native';

const MovieListEmpty = () => (
	<View style={styles.container}>
		<View style={styles.textbox}>
			<Text style={styles.textbox}>
				Það eru engar sýningar að þessu sinni í dag.
			</Text>
		</View>
	</View>
);

export default MovieListEmpty;

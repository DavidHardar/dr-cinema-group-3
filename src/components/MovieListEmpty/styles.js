import {StyleSheet} from 'react-native';
import {GREY} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		padding: 10,
		backgroundColor: GREY,
		marginTop: 15,
		borderRadius: 12,
		borderColor: 'black',
		borderWidth: 3,
	},

	textbox: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row',
		width: '95%',
		alignItems: 'center',
		textAlign: 'center',
		textAlignVertical: 'center',
		fontSize: 20,
		fontFamily: 'Arvo-Regular',
	},

});

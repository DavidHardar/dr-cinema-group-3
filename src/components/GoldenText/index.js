import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import {GOLD_1, GOLD_2, GOLD_3} from '../../styles/colors';
import MaskedView from '@react-native-community/masked-view';

const GoldenText = ({text}) => (
	<View style={ styles.container }>
		<MaskedView
			style={ styles.masked }
			maskElement={
				<View style={styles.goldenView}>
					<Text style={styles.goldenText}>
						{text}
					</Text>
				</View>
			} >
			<LinearGradient
				colors={[GOLD_1, GOLD_2, GOLD_2, GOLD_3, GOLD_1, GOLD_2, GOLD_2, GOLD_3]}
				start={{x: 1, y: 1}}
				end={{x: 0, y: 0.33}}
				style={styles.linear} />
		</MaskedView>
	</View>
);

GoldenText.propTypes = {
	text: PropTypes.string.isRequired,
};

export default GoldenText;

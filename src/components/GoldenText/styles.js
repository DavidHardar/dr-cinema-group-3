import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
	},
	masked: {
		height: 60,
		width: 300,
	},
	goldenView: {
		backgroundColor: 'transparent',
		flex: 1,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'flex-start',
	},
	goldenText: {
		fontSize: 30,
		color: 'black',
		fontFamily: 'Limelight',
	},
	linear: {
		flex: 1,
	},

});

import React from 'react';
import styles from './styles';
import Cinema from '../Cinema';
import EmptyList from '../ListEmpty';
import {useSelector, useDispatch} from 'react-redux';
import {getCinemas} from '../../actions/getCinemasAction';
import {View, FlatList, RefreshControl} from 'react-native';
import {setCinemaRefresh} from '../../actions/refreshCinemaAction';

const CinemaList = () => {
	const cinemaRefresh = useSelector(state => state.cinemaRefresh);
	const dispatch = useDispatch();
	const cinemas = useSelector(state => state.cinemas);

	const onRefresh = React.useCallback(async () => {
		dispatch(setCinemaRefresh(true));
		await dispatch(getCinemas());
		dispatch(setCinemaRefresh(false));
	}, [cinemaRefresh]);

	return (
		<View style={styles.container}>
			<FlatList data={cinemas}
				style={styles.flatlist}
				ListEmptyComponent={<EmptyList/>}
				refreshControl={
					<RefreshControl
						refreshing={cinemaRefresh}
						onRefresh={onRefresh} />
				}
				renderItem={({item}) => (

					<Cinema
						id = {item.id}
						name = {item.name}
						website = {item.website}
					/>

				)}
				keyExtractor={cinemas => cinemas.id} />

		</View>
	);
};

export default CinemaList;

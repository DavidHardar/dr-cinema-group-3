import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {token} from '../../services/apiService';
import {useNavigation} from '@react-navigation/native';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {UPCOMING_MOVIE_DETAILS, MONTH_ARRAY} from '../../constants';

const UpcomingMovie = ({id}) => {
	const {navigate} = useNavigation();
	const upcomingMovie = useSelector(state =>
		state.upcomingMovies.find(upcomingMovie =>
			upcomingMovie.id === id),
	);
	const [year, month, day] = upcomingMovie['release-dateIS'].split('-');
	const monthName = MONTH_ARRAY[parseInt(month, 10)];
	const dateString = day.concat('. ', monthName, ' ', year);
	return (
		<TouchableOpacity
			style={styles.touchable}
			onPress={() => {
				navigate(UPCOMING_MOVIE_DETAILS, {
					id,
				});
			}} >
			<View style={ styles.container }>
				<View style={ styles.textContainer}>
					<View style={ styles.titleContainer }>
						<Text style={ styles.title }>{ upcomingMovie.title }</Text>
					</View>
					<View style={ styles.releaseDateContainer }>
						<Text style={ styles.releaseDate }>Útgáfudagur: { dateString }</Text>
					</View>
				</View>
				<Image
					style={ styles.thumbnail }
					source={{uri: upcomingMovie.poster,
						headers: {
							'x-access-token': token},
					}} />
			</View>
		</TouchableOpacity>);
};

UpcomingMovie.propTypes = {
	id: PropTypes.number.isRequired,
};

export default UpcomingMovie;

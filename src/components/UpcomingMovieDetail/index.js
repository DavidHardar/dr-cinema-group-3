import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {View, Text, Image} from 'react-native';
import {token} from '../../services/apiService';

const UpcomingMovieDetail = ({upcomingMovie}) => {
	let genreString = 'Tegund: ';
	upcomingMovie.genres.forEach((genre, index) => {
		if (typeof (genre) === 'object') {
			if (index === 0) {
				genreString = genreString.concat(genre.Name);
			} else if (index < upcomingMovie.genres.length - 1) {
				genreString = genreString.concat(', ', genre.Name);
			} else {
				genreString = genreString.concat(' og ', genre.Name);
			}
		}
	});
	return (
		<View>
			<View style={ styles.container }>
				<View style={ styles.textContainer}>
					<View style={ styles.timeContainer }>
						<Text style={ styles.time }>{upcomingMovie.title}</Text>
					</View>
					<View style={ styles.auditoriumContainer }>
						<Text style={ styles.auditorium }>Útgáfuár: {upcomingMovie.year}</Text>
					</View>
					<View style={ styles.auditoriumContainer }>
						<Text style={ styles.auditorium }>Lýsing: {upcomingMovie.plot}</Text>
					</View>
					<View style={ styles.auditoriumContainer }>
						<Text style={ styles.auditorium }>{genreString}</Text>
					</View>
				</View>
				<Image
					style={ styles.thumbnail }
					source={{uri: upcomingMovie.poster,
						headers: {
							'x-access-token': token},
					}} />
			</View>
		</View>
	);
};

UpcomingMovieDetail.propTypes = {
	upcomingMovie: PropTypes.object.isRequired,
};

export default UpcomingMovieDetail;

import PropTypes from 'prop-types';
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {ScrollView, RefreshControl} from 'react-native';
import {resetIndex} from '../../actions/getNextYTVideoAction';
import RotationalYouTubePlayer from '../RotationalYoutubePlayer';
import UpcomingMovieDetail from '../../components/UpcomingMovieDetail';
import {getUpcomingMovies} from '../../actions/getUpcomingMoviesAction';
import {setUpcomingDetailsRefresh} from '../../actions/refreshUpcomingDetailsAction';

const TrailerList = ({id}) => {
	const dispatch = useDispatch();

	const upcomingDetailsRefresh = useSelector(state => state.upcomingDetailsRefresh);

	useEffect(() => {
		dispatch(resetIndex());
	}, []);

	const onRefresh = React.useCallback(async () => {
		dispatch(setUpcomingDetailsRefresh(true));
		dispatch(getUpcomingMovies());
		dispatch(setUpcomingDetailsRefresh(false));
	}, [upcomingDetailsRefresh]);

	const upcomingMovie = useSelector(state =>
		state.upcomingMovies.find(upcomingMovie =>
			upcomingMovie.id === id),
	);
	let videoIds = [];
	// Check if movie has trailer (Most of prepwork done in cleanerService)
	if (!(upcomingMovie.trailers === null)) {
		const trailers = upcomingMovie.trailers[0].results;
		// Make a list of videoIds gotten from urls
		videoIds = trailers.map(trailer => {
			const urlSplit = trailer.url.split('/');
			return urlSplit[urlSplit.length - 1].split('?')[0];
		});
	} else {
		videoIds = [];
	}

	return (
		<ScrollView
			refreshControl={
				<RefreshControl
					refreshing={upcomingDetailsRefresh}
					onRefresh={onRefresh} />
			} >
			<UpcomingMovieDetail upcomingMovie={ upcomingMovie }/>
			<RotationalYouTubePlayer videoIds={videoIds} />
		</ScrollView>
	);
};

TrailerList.propTypes = {
	id: PropTypes.number.isRequired,
};

export default TrailerList;

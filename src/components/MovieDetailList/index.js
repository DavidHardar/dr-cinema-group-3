import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import Showtime from '../../components/Showtime';
import {useSelector, useDispatch} from 'react-redux';
import MovieDetail from '../../components/MovieDetail';
import {getMovies} from '../../actions/getMoviesAction';
import {View, FlatList, RefreshControl} from 'react-native';
import {setMovieRefresh} from '../../actions/refreshMovieAction';

const MovieDetailList = ({movieId, cinemaId}) => {
	const movie = useSelector(state =>
		state.movies.find(movies =>
			movies.id === movieId),
	);
	const cinemaShowtimes = movie.showtimes.find(showtime => showtime.cinema.id === cinemaId);

	const movieRefresh = useSelector(state => state.movieRefresh);

	const dispatch = useDispatch();
	const onRefresh = React.useCallback(async () => {
		dispatch(setMovieRefresh(true));
		await dispatch(getMovies());
		dispatch(setMovieRefresh(false));
	}, [movieRefresh]);

	return (
		<View style={styles.container}>

			<FlatList
				refreshControl={
					<RefreshControl
						refreshing={movieRefresh}
						onRefresh={onRefresh} />
				}
				style={styles.list}
				data = {cinemaShowtimes.schedule}
				ListHeaderComponent={
					<MovieDetail id={movieId}/>
				}
				renderItem={({item}) => (
					<Showtime
						time={item.time}
						purchaseUrl={item.purchase_url} />
				)}
				keyExtractor={schedule => schedule.time} />
		</View>
	);
};

MovieDetailList.propTypes = {
	movieId: PropTypes.number.isRequired,
	cinemaId: PropTypes.number.isRequired,
};

export default MovieDetailList;

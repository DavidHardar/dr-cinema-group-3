import {StyleSheet} from 'react-native';
import {LIGHTBLUE, WHITE} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		padding: 10,
		width: '100%',
		overflow: 'hidden',
		flexDirection: 'column',
		justifyContent: 'center',
	},

	image: {
		width: 80,
		height: 20,
		resizeMode: 'contain',
		alignItems: 'flex-start',
	},

	title: {
		flex: 1,
		flexGrow: 1,
		fontSize: 24,
		textAlign: 'left',
		flexDirection: 'row',
		fontFamily: 'Arvo-Bold',
		textAlignVertical: 'auto',
	},

	titleContainer: {
		flex: 1,
		flexGrow: 1,
		flexDirection: 'row',
		alignItems: 'flex-start',
		justifyContent: 'flex-start',
	},

	website: {
		flex: 0.5,
		flexDirection: 'row',
		marginLeft: 'auto',
		width: '40%',
		fontSize: 15,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: LIGHTBLUE,
		borderRadius: 12,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 24,

	},
	websitetext: {
		textAlignVertical: 'auto',
		textAlign: 'center',
		color: WHITE,
	},

	touchable: {
		flex: 0.5,
		width: '100%',
		height: 120,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		backgroundColor: WHITE,
		marginTop: 15,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 24,
	},

	thumbnail: {
		width: 20,
		aspectRatio: 10 / 16,
	},
});

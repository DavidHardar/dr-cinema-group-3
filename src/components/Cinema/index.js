import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {CINEMA_DETAILS} from '../../constants';
import {useNavigation} from '@react-navigation/native';
import {View, Text, TouchableOpacity, Linking, Image} from 'react-native';

const Cinema = ({id}) => {
	const cinema = useSelector(state =>
		state.cinemas.find(cinema =>
			cinema.id === id),
	);
	const {navigate} = useNavigation();
	return (
		<TouchableOpacity

			style={styles.touchable}

			onPress={() => {
				navigate(CINEMA_DETAILS, {
					id,

				});
			}}>

			<View style={ styles.container }>
				<View style = {{flex: 0.2, flexDirection: 'row-reverse'}}>
					<Image style= {styles.image} source={{uri: cinema.logo}}/>
				</View>
				<View style={ styles.titleContainer }>
					<Text style={ styles.title }>{ cinema.name }</Text>
				</View>
				<TouchableOpacity onPress={() => Linking.openURL('http://'.concat(cinema.website))} style={ styles.website }>
					<Text style={ styles.websitetext } >{ cinema.website }</Text>
				</TouchableOpacity>

			</View>
		</TouchableOpacity>);
};

Cinema.propTypes = {
	id: PropTypes.number.isRequired,
	name: PropTypes.string.isRequired,
	website: PropTypes.string.isRequired,
};

export default Cinema;

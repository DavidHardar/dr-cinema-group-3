import {StyleSheet} from 'react-native';
import {DARKGREY, OTHERGREY} from '../../styles/colors';

export default StyleSheet.create({
	touchable: {
		flex: 1,
		padding: 15,
		backgroundColor: OTHERGREY,
		borderColor: DARKGREY,
		width: '95%',
		borderWidth: 3,
		margin: 10,
		borderRadius: 15,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},

	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
	},

	textContainer: {
		flex: 1,
		flexBasis: '60%',
	},

	titleContainer: {
		flex: 1,
		justifyContent: 'center',
	},

	title: {
		textAlignVertical: 'center',
		fontSize: 16,
		fontFamily: 'Arvo-Bold',
	},

	releaseDateContainer: {
		flex: 1,
		justifyContent: 'center',
	},

	releaseDate: {
		textAlignVertical: 'center',
		fontFamily: 'Arvo-Regular',
	},

	thumbnail: {
		width: 80,
		aspectRatio: 10 / 16,
	},

});

import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {MOVIE_DETAILS} from '../../constants';
import {token} from '../../services/apiService';
import {useNavigation} from '@react-navigation/native';
import {View, Text, Image, TouchableOpacity} from 'react-native';

const Movie = ({cinemaId, movieId}) => {
	const {navigate} = useNavigation();
	const movie = useSelector(state =>
		state.movies.find(movie =>
			movie.id === movieId),
	);

	let genreString = 'Tegund: ';
	movie.genres.forEach((genre, index) => {
		if (index === 0) {
			genreString = genreString.concat(genre.Name);
		} else if (index < movie.genres.length - 1) {
			genreString = genreString.concat(', ', genre.Name);
		} else {
			genreString = genreString.concat(' og ', genre.Name);
		}
	});

	return (
		<TouchableOpacity
			style={styles.touchable}
			onPress={() => {
				navigate(MOVIE_DETAILS, {
					cinemaId,
					movieId,

				});
			}} >

			<View style={ styles.container }>
				<View style={ styles.textContainer}>
					<View style={ styles.titleContainer }>
						<Text style={ styles.title }>{ movie.title }</Text>
					</View>
					<View style={ styles.titleContainer }>
						<Text style={ styles.releaseDate }>Útgáfuár: { movie.year }</Text>
					</View>
					<View style={ styles.releaseDateContainer }>
						<Text style={ styles.releaseDate }>{ genreString }</Text>
					</View>
				</View>
				<Image
					style={ styles.thumbnail }
					source={{uri: movie.poster,
						headers: {
							'x-access-token': token},
					}} />
			</View>

		</TouchableOpacity>);
};

Movie.propTypes = {
	cinemaId: PropTypes.number.isRequired,
	movieId: PropTypes.number.isRequired,
};

export default Movie;

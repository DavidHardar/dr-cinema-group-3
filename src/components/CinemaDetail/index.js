import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {Feather} from '@expo/vector-icons';
import {View, Image, Text, Linking, TouchableOpacity} from 'react-native';

const CinemaDetail = ({id}) => {
	const cinema = useSelector(state =>
		state.cinemas.find(cinema =>
			cinema.id === id),
	);
	return (
		<View style={styles.container}>
			<View style={styles.name}>
				<Text style={styles.headertext}>{cinema.name}</Text>
				<View style = {styles.imageview}>
					<Image style= {styles.image} source={{uri: cinema.logo}}/>
				</View>
			</View>

			<View style={styles.largetext}>
				<Text style={styles.text}>Lýsing: {cinema.description}</Text>
			</View>
			<View style={styles.smalltext}>
				<Text style={styles.text}>Vefsiða: </Text><Text style={styles.link} onPress={() => Linking.openURL('https://'.concat(cinema.website))} >{cinema.website}</Text>
			</View>
			<View style={[styles.smalltext, {alignItems: 'center'}]}>
				<Text style={styles.phonetext} >Símanúmer: </Text><Text style={styles.text}>{cinema.phone}</Text>
				<TouchableOpacity style = {styles.touch}>
					<Feather onPress={() => {
						Linking.openURL(`tel:${cinema.phone}`);
					}} name="phone-call" size={24} color="white" />
				</TouchableOpacity>
			</View>
			<View style={styles.smalltext}>
				<Text style={styles.text}>Heimilisfang: {cinema.address}, {cinema.city}</Text>
			</View>

		</View>
	);
};

CinemaDetail.propTypes = {
	id: PropTypes.number.isRequired,
};

export default CinemaDetail;

import {StyleSheet} from 'react-native';
import {WHITE} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-around',
		width: '100%',
		padding: 10,
		backgroundColor: WHITE,
		borderWidth: 3,
		borderBottomEndRadius: 12,
		borderBottomStartRadius: 12,
	},

	name: {
		flex: 1,
		flexDirection: 'row',
		flexBasis: 'auto',
		width: '100%',
		padding: 10,
	},

	smalltext: {
		flex: 1,
		flexDirection: 'row',
		flexBasis: 'auto',
		width: '100%',
		padding: 10,
	},

	largetext: {
		flex: 1,
		flexDirection: 'row',
		flexBasis: 'auto',
		flexGrow: 1,
		width: '100%',
		padding: 10,
		borderColor: 'black',
		borderRadius: 12,
		borderWidth: 1,
	},

	touch: {
		flexDirection: 'row',
		resizeMode: 'contain',
		justifyContent: 'flex-end',
		marginLeft: 'auto',
		flexBasis: 'auto',
		backgroundColor: 'green',
		padding: 5,
		borderRadius: 100,
		borderWidth: 1,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 24,
	},

	text: {
		fontFamily: 'Arvo-Regular',
	},

	phonetext: {
		fontFamily: 'Arvo-Regular',
		textAlign: 'center',
	},

	headertext: {
		fontFamily: 'Arvo-Bold',
	},

	link: {
		color: 'blue',
		fontFamily: 'Arvo-Regular',
	},

	image: {
		alignItems: 'flex-start',
		width: 80,
		height: 20,
		resizeMode: 'contain',
	},

	imageview: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
	},

});

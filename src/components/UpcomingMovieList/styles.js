import {StyleSheet} from 'react-native';

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
	},

	list: {
		flex: 1,
		width: '100%',
	},

});

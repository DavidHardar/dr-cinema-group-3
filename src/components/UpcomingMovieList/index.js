import styles from './styles';
import EmptyList from '../ListEmpty';
import React, {useEffect} from 'react';
import UpcomingMovie from '../UpcomingMovie';
import {useDispatch, useSelector} from 'react-redux';
import {View, RefreshControl, FlatList} from 'react-native';
import {setUpcomingRefresh} from '../../actions/refreshUpcomingAction';
import {getUpcomingMovies} from '../../actions/getUpcomingMoviesAction';

const UpcomingMovieList = () => {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(getUpcomingMovies());
	}, []);
	const upcomingRefresh = useSelector(state => state.upcomingRefresh);
	const upcomingMovies = useSelector(state => state.upcomingMovies);

	const onRefresh = React.useCallback(async () => {
		dispatch(setUpcomingRefresh(true));
		dispatch(getUpcomingMovies());
		dispatch(setUpcomingRefresh(false));
	}, [upcomingRefresh]);

	return (
		<View style={ styles.container }>
			<FlatList
				ListEmptyComponent={<EmptyList/>}
				refreshControl={
					<RefreshControl
						refreshing={upcomingRefresh}
						onRefresh={onRefresh} />
				}
				style={styles.list}
				data = {upcomingMovies}
				renderItem={({item}) => (
					<UpcomingMovie id={item.id} />
				)}
				keyExtractor={upcomingMovies => upcomingMovies.id} />
		</View>
	);
};

export default UpcomingMovieList;

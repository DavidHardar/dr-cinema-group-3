import {StyleSheet} from 'react-native';
import {GREY} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		width: '100%',
		backgroundColor: GREY,
		borderRadius: 12,
		borderColor: 'black',
		borderWidth: 3,
	},

	textbox: {
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'row',
		width: '95%',
		alignItems: 'center',
	},

	linkbox: {
		flex: 1,
		flexDirection: 'row',
		width: '95%',
		justifyContent: 'center',
		alignItems: 'center',
	},

	link: {
		color: 'blue',
		textDecorationLine: 'underline',
		textDecorationColor: 'blue',
	},

	image: {
		resizeMode: 'contain',
		width: 80,
		height: 80,
		aspectRatio: 1 / 1,
	},

});

import React from 'react';
import styles from './styles';
import error from '../../../assets/error.png';
import {View, Text, Linking, Image} from 'react-native';

const EmptyList = () => (
	<View style={styles.container}>
		<Image source={error} style={styles.image}/>
		<View style={styles.textbox}>
			<Text style={styles.textbox}>
                    Appið nær ekki að sækja gögnin eins og er.
                    Vinsamlegast reyndu að endurræsa appið, ef það virkar ekki, endilega hafðu
                    samband við okkur á:
			</Text>
		</View>
		<View style={styles.linkbox}>
			<Text onPress={() => Linking.openURL('https://help.ru.is/')} style={styles.link} >help.ru.is</Text>
		</View>
	</View>
);

export default EmptyList;

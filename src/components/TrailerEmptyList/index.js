import React from 'react';
import styles from './styles';
import {View, Text} from 'react-native';

const MovieListEmpty = () => (
	<View style={styles.container}>
		<View style={styles.textbox}>
			<Text style={styles.textbox}>
                    D&apos;oh! Það eru engar stiklur til fyrir þessa mynd. :(
			</Text>
		</View>
	</View>
);

export default MovieListEmpty;

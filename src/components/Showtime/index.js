import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {View, Text, TouchableOpacity, Linking} from 'react-native';

const Showtime = ({time, purchaseUrl}) => {
	let [displayedTime, auditoriumNumber] = time.split(' ');
	displayedTime = displayedTime.trim();
	auditoriumNumber = auditoriumNumber.trim().replace('(', '').replace(')', '');
	const auditorium = 'Salur ' + auditoriumNumber;
	return (

		<View style={ styles.container }>
			<View style={ styles.textContainer}>
				<View style={ styles.timeContainer }>
					<Text style={ styles.time }>{ displayedTime }</Text>
				</View>
				<View style={ styles.auditoriumContainer }>
					<Text style={ styles.auditorium }>{ auditorium }</Text>
				</View>
			</View>
			<TouchableOpacity
				style={styles.button}
				onPress={() => Linking.openURL(purchaseUrl)} >
				<Text style={styles.buttonText}>Kaupa miða</Text>
			</TouchableOpacity>
		</View>
	);
};

Showtime.propTypes = {
	time: PropTypes.string.isRequired,
	purchaseUrl: PropTypes.string.isRequired,
};

export default Showtime;

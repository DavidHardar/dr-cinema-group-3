import {StyleSheet} from 'react-native';
import {WHITE, LIGHTBLUE} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		padding: 15,
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'space-around',
		backgroundColor: WHITE,
		width: '100%',
		maxWidth: '100%',
		margin: 10,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},

	textContainer: {
		flex: 1,
	},

	timeContainer: {
		flex: 1,
		justifyContent: 'center',
	},

	time: {
		textAlignVertical: 'center',
		fontSize: 16,
		fontFamily: 'Arvo-Regular',
	},

	auditoriumContainer: {
		flex: 1,
		justifyContent: 'center',
	},

	auditorium: {
		textAlignVertical: 'center',
		fontFamily: 'Arvo-Regular',
	},

	button: {
		flex: 1,
		width: 20,
		height: 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: LIGHTBLUE,
		borderRadius: 12,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 12,
		},
		shadowOpacity: 0.58,
		shadowRadius: 16.00,
		elevation: 24,
	},

	buttonText: {
		textAlignVertical: 'auto',
		textAlign: 'center',
		color: WHITE,
	},

});

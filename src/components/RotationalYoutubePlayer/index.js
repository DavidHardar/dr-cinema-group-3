import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {View, Text} from 'react-native';
import {WHITE} from '../../styles/colors';
import {AntDesign} from '@expo/vector-icons';
import TrailerEmptyList from '../TrailerEmptyList';
import {useSelector, useDispatch} from 'react-redux';
import YoutubePlayer from 'react-native-youtube-iframe';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {getNextVideo, getPrevVideo} from '../../actions/getNextYTVideoAction';

const RotationalYouTubePlayer = ({videoIds}) => {
	const limit = videoIds.length - 1;
	const dispatch = useDispatch();
	const ytIndex = useSelector(state => state.ytIndex);
	const show = (limit >= 0);
	const showBtn = (limit > 1);

	return (
		<View>
			{
				show
					? <View >
						<YoutubePlayer
							height={240}
							play={false}
							videoId={videoIds[ytIndex]} />
						{
							showBtn
								? <View style={styles.goodView}>
									<TouchableOpacity
										style={styles.buttonback}
										onPress={() => {
											dispatch(getPrevVideo(limit));
										}} >
										<View style={styles.iconview}>
											<Text style={styles.buttonText}>Fyrri stikla</Text>
											<AntDesign name="stepbackward" size={24} color={WHITE} />
										</View>
									</TouchableOpacity>
									<TouchableOpacity
										style={styles.buttonforward}
										onPress={() => {
											dispatch(getNextVideo(limit));
										}} >
										<View style={styles.iconview}>
											<Text style={styles.buttonText}>Næsta stikla</Text>
											<AntDesign name="stepforward" size={24} color={WHITE} />
										</View>
									</TouchableOpacity>
								</View>
								: <></>
						}
					</View>
					: <TrailerEmptyList />
			}
		</View>
	);
};

RotationalYouTubePlayer.propTypes = {
	videoIds: PropTypes.array.isRequired,
};

export default RotationalYouTubePlayer;

import {StyleSheet} from 'react-native';
import {WHITE, LIGHTBLUE} from '../../styles/colors';

export default StyleSheet.create({
	goodView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
	},

	buttonforward: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center',
		alignSelf: 'center',
		marginBottom: 215,
		padding: 10,
		backgroundColor: LIGHTBLUE,
		borderRadius: 12,
	},

	buttonback: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-start',
		alignItems: 'center',
		alignSelf: 'center',
		marginBottom: 215,
		padding: 10,
		backgroundColor: LIGHTBLUE,
		borderRadius: 12,
	},

	buttonText: {
		textAlignVertical: 'center',
		color: WHITE,
	},

	iconview: {
		alignItems: 'center',
	},
});

import styles from './styles';
import {loadAsync} from 'expo-font';
import PropTypes from 'prop-types';
import {hideAsync} from 'expo-splash-screen';
import {getGenres, authentication} from '../../services/apiService';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies} from '../../actions/getMoviesAction';
import React, {useEffect, useRef, useMemo} from 'react';
import {Animated, StyleSheet, View} from 'react-native';
import {getCinemas} from '../../actions/getCinemasAction';
import {setAppReady} from '../../actions/setAppReadyAction';
import {setSplashAnimation} from '../../actions/setSplashAnimation';
import {getUpcomingMovies} from '../../actions/getUpcomingMoviesAction';

// This function is heavily influenced by https://github.com/expo/examples/tree/master/with-splash-screen
function AnimatedSplashScreen({children, image}) {
	const animation = useRef(new Animated.Value(1)).current;
	const appLoading = useSelector(state => state.appLoading);
	const splashAnimation = useSelector(state => state.splashAnimation);
	const dispatch = useDispatch();

	useEffect(() => {
		if (appLoading) {
			Animated.timing(animation, {
				toValue: 0,
				duration: 1000,
				useNativeDriver: true,
			}).start(() => dispatch(setSplashAnimation()));
		}
	}, [appLoading]);

	const onImageLoaded = useMemo(() => async () => {
		try {
			await hideAsync();
			// Load stuff
			const customFonts = {
				'Arvo-Regular': require('../../../assets/fonts/Arvo-Regular.ttf'),
				'Arvo-Bold': require('../../../assets/fonts/Arvo-Bold.ttf'),
				Limelight: require('../../../assets/fonts/Limelight-Regular.ttf'),
			};
			await loadAsync(customFonts);
			await authentication();
			await getGenres();
			await dispatch(getCinemas());
			await dispatch(getMovies());
			await dispatch(getUpcomingMovies());
		} catch (err) {
			console.log('ERROR IN STARTUP: ', err);
		} finally {
			dispatch(setAppReady());
		}
	});

	return (
		<View style={{flex: 1}}>
			{appLoading && children}
			{!splashAnimation && (
				<Animated.View
					pointerEvents="none"
					style={[
						StyleSheet.absoluteFill,
						{
							// BackgroundColor: "red",
							opacity: animation,
							transform: [
								{
									rotate: animation.interpolate({
										inputRange: [0, 1],
										outputRange: ['0deg', '1440deg'],
									}),
								},
							],
						},
					]} >
					<Animated.Image
						style={[
							styles.image,
							{transform: [
								{
									scale: animation,
								},
							],
							},
						]}
						source={image}
						onLoadEnd={onImageLoaded}
						fadeDuration={0} />
				</Animated.View>
			)}
		</View>
	);
}

AnimatedSplashScreen.propTypes = {
	children: PropTypes.array.isRequired,
	image: PropTypes.number.isRequired,
};

export default AnimatedSplashScreen;

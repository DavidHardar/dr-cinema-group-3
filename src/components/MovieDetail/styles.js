import {StyleSheet} from 'react-native';
import {DARKGREY, OTHERGREY} from '../../styles/colors';

export default StyleSheet.create({
	container: {
		flex: 1,
		padding: 15,
		alignSelf: 'center',
		flexDirection: 'row',
		justifyContent: 'space-around',
		backgroundColor: OTHERGREY,
		borderColor: DARKGREY,
		width: '100%',
		borderWidth: 3,
		borderBottomEndRadius: 12,
		borderBottomStartRadius: 12,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
	},

	textContainer: {
		flex: 1,
		flexBasis: '60%',
	},

	timeContainer: {
		flex: 1,
		justifyContent: 'center',
		flexBasis: 'auto',
		width: '100%',
		padding: 5,
	},

	time: {
		textAlignVertical: 'center',
		fontSize: 16,
		fontFamily: 'Arvo-Bold',
	},

	auditoriumContainer: {
		flex: 1,
		justifyContent: 'center',
		padding: 5,
		width: '100%',
	},

	auditorium: {
		textAlignVertical: 'center',
		fontFamily: 'Arvo-Regular',
	},

	thumbnail: {
		width: '40%',
		aspectRatio: 10 / 16,
	},

	image: {
		flex: 1,
		backgroundColor: 'yellow',
	},

});

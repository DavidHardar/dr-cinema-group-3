import React from 'react';
import styles from './styles';
import PropTypes from 'prop-types';
import {useSelector} from 'react-redux';
import {View, Text, Image} from 'react-native';
import {token} from '../../services/apiService';

const MovieDetail = ({id}) => {
	const movie = useSelector(state =>
		state.movies.find(movie =>
			movie.id === id),
	);

	let genreString = 'Tegund: ';
	movie.genres.forEach((genre, index) => {
		if (index === 0) {
			genreString = genreString.concat(genre.Name);
		} else if (index < movie.genres.length - 1) {
			genreString = genreString.concat(', ', genre.Name);
		} else {
			genreString = genreString.concat(' og ', genre.Name);
		}
	});
	return (
		<View style={ styles.container }>
			<View style={ styles.textContainer}>
				<View style={ styles.timeContainer }>
					<Text style={ styles.time }>{movie.title}</Text>
				</View>
				<View style={ styles.auditoriumContainer }>
					<Text style={ styles.auditorium }>Lengd: {movie.durationMinutes} mínútur</Text>
				</View>
				<View style={ styles.auditoriumContainer }>
					<Text style={ styles.auditorium }>Útgáfuár: {movie.year}</Text>
				</View>
				<View style={ styles.auditoriumContainer }>
					<Text style={ styles.auditorium }>{movie.plot}</Text>
				</View>
				<View style={ styles.auditoriumContainer }>
					<Text style={ styles.auditorium }>{genreString}</Text>
				</View>
			</View>
			<Image
				style={ styles.thumbnail }
				source={{uri: movie.poster,
					headers: {
						'x-access-token': token},
				}} />
		</View>
	);
};

MovieDetail.propTypes = {
	id: PropTypes.number.isRequired,
};

export default MovieDetail;

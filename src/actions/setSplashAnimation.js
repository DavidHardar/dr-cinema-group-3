import {SPLASH_ANIMATION_DONE} from '../constants';

export const setSplashAnimation = () => ({
	type: SPLASH_ANIMATION_DONE,
	payload: true,
});

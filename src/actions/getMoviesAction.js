import {UPDATE_MOVIES} from '../constants';
import * as filterService from '../services/filterService';

export const getMovies = () => async dispatch => {
	try {
		const movies = await filterService.movieFilter();
		dispatch(getMoviesSuccess(movies));
	} catch (err) {
		console.log('ERROR IN ACTIONS: ', err);
	}
};

const getMoviesSuccess = movies => ({
	type: UPDATE_MOVIES,
	payload: movies,
});

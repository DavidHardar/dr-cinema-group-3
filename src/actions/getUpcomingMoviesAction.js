import {UPDATE_UPCOMING_MOVIES} from '../constants';
import * as filterService from '../services/filterService';

export const getUpcomingMovies = () => async dispatch => {
	try {
		const upcomingMovies = await filterService.upcomingMovieFilter();
		dispatch(getUpcomingMoviesSuccess(upcomingMovies));
	} catch (err) {
		console.log('ERROR IN ACTIONS: ', err);
	}
};

const getUpcomingMoviesSuccess = upcomingMovies => ({
	type: UPDATE_UPCOMING_MOVIES,
	payload: upcomingMovies,
});

import {REFRESH_CINEMA} from '../constants';

export const setCinemaRefresh = bool => ({
	type: REFRESH_CINEMA,
	payload: bool,
});

import {NEXT_VIDEO, PREV_VIDEO, RESET_INDEX} from '../constants';

export const getNextVideo = limit => ({
	type: NEXT_VIDEO,
	payload: limit,
});

export const getPrevVideo = limit => ({
	type: PREV_VIDEO,
	payload: limit,
});

export const resetIndex = () => ({
	type: RESET_INDEX,
	payload: 0,
});

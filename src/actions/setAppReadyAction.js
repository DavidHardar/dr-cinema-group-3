import {APP_DONE_LOADING} from '../constants';

export const setAppReady = () => ({
	type: APP_DONE_LOADING,
	payload: true,
});

import {REFRESH_MOVIE} from '../constants';

export const setMovieRefresh = bool => ({
	type: REFRESH_MOVIE,
	payload: bool,
});

import {REFRESH_CINEMA_DETAILS} from '../constants';

export const setCinemaDetailsRefresh = bool => ({
	type: REFRESH_CINEMA_DETAILS,
	payload: bool,
});

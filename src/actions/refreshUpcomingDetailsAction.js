import {REFRESH_UPCOMING_MOVIE} from '../constants';

export const setUpcomingDetailsRefresh = bool => ({
	type: REFRESH_UPCOMING_MOVIE,
	payload: bool,
});

import {REFRESH_UPCOMING} from '../constants';

export const setUpcomingRefresh = bool => ({
	type: REFRESH_UPCOMING,
	payload: bool,
});

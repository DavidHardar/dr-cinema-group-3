import {UPDATE_CINEMAS} from '../constants';
import * as filterService from '../services/filterService';

export const getCinemas = () => async dispatch => {
	try {
		const cinemas = await filterService.cinemaFilter();
		dispatch(getCinemasSuccess(cinemas));
	} catch (err) {
		console.log('ERROR IN ACTIONS: ', err);
	}
};

const getCinemasSuccess = cinemas => ({
	type: UPDATE_CINEMAS,
	payload: cinemas,
});

import React from 'react';
import UpcomingView from '../views/UpcomingView';
import GoldenText from '../components/GoldenText';
import {createStackNavigator} from '@react-navigation/stack';
import {UPCOMING, UPCOMING_MOVIE_DETAILS} from '../constants';
import UpcomingDetailsView from '../views/UpcomingDetailsView';

const UpcomingStack = createStackNavigator();

export default function UpcomingStackScreen() {
	return (
		<UpcomingStack.Navigator>
			<UpcomingStack.Screen
				name={UPCOMING}
				component={UpcomingView}
				options={{
					title: '     ',
					headerTitle: () => <GoldenText text={'Væntanlegt í bíó'}/>,
					headerStyle: {backgroundColor: '#000'},
				}} />
			<UpcomingStack.Screen
				name={UPCOMING_MOVIE_DETAILS}
				component={UpcomingDetailsView}
				options={{
					title: '     ',
					headerTitle: () => <GoldenText text={'Stiklur'}/>,
					headerStyle: {backgroundColor: '#000'},
					headerTintColor: '#fff',
				}} />
		</UpcomingStack.Navigator>
	);
}

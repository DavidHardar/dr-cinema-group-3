import React from 'react';
import CinemasView from '../views/CinemasView';
import GoldenText from '../components/GoldenText';
import MovieDetailView from '../views/MovieDetailView';
import CinemaDetailsView from '../views/CinemaDetailView';
import {createStackNavigator} from '@react-navigation/stack';
import {CINEMAS, CINEMA_DETAILS, MOVIE_DETAILS} from '../constants';

const CinemaStack = createStackNavigator();

export default function CinemaStackScreen() {
	return (
		<CinemaStack.Navigator>
			<CinemaStack.Screen
				name={CINEMAS}
				component={CinemasView}
				options={{
					title: '     ', // To trick ios arrow
					headerTitle: () => <GoldenText text={'Bíó'}/>,
					headerStyle: {backgroundColor: '#000'},
				}} />
			<CinemaStack.Screen
				name={CINEMA_DETAILS}
				component={CinemaDetailsView}
				options={{
					title: '     ',
					headerTitle: () => <GoldenText text={'Upplýsingar'}/>,
					headerStyle: {backgroundColor: '#000'},
					headerTintColor: '#fff',
				}} />
			<CinemaStack.Screen
				name={MOVIE_DETAILS}
				component={MovieDetailView}
				options={{
					title: '     ',
					headerTitle: () => <GoldenText text={'Kaupa miða'}/>,
					headerStyle: {backgroundColor: '#000'},
					headerTintColor: '#fff',
				}} />
		</CinemaStack.Navigator>

	);
}

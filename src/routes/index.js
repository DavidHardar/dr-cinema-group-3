import React from 'react';
import {FontAwesome} from '@expo/vector-icons';
import {LinearGradient} from 'expo-linear-gradient';
import CinemaStackScreen from './CinemaStackScreen';
import UpcomingStackScreen from './UpcomingStackScreen';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {FIRST_GRADIENT_COLOR, SECOND_GRADIENT_COLOR, LAST_GRADIENT_COLOR} from '../styles/colors';

const Tab = createBottomTabNavigator();

export default function AppContainer() {
	const mainTheme = {
		...DefaultTheme,
		colors: {
			...DefaultTheme.colors,
			background: 'transparent',
		},
	};
	return (
		<LinearGradient colors={[FIRST_GRADIENT_COLOR, SECOND_GRADIENT_COLOR, LAST_GRADIENT_COLOR]} style={{flex: 1}}>
			<NavigationContainer theme={mainTheme}>
				<Tab.Navigator screenOptions = {({route}) => ({
					tabBarIcon: ({color, size}) => {
						let iconName;
						if (route.name === 'Cinemas Tab') {
							iconName = 'ticket';
						} else if (route.name === 'Upcoming Movies Tab') {
							iconName = 'film';
						}

						return <FontAwesome name={iconName} size={size} color={color} />;
					},
					tabBarActiveTintColor: 'red',
					tabBarInactiveTintColor: 'gray',
					tabBarInactiveBackgroundColor: '#0f0f0f10',
					headerShown: false,
				})}>
					<Tab.Screen
						name="Cinemas Tab"
						component={CinemaStackScreen}
						options={{title: 'Bíó'}} />
					<Tab.Screen
						name="Upcoming Movies Tab"
						component={UpcomingStackScreen}
						options={{title: 'Væntanlegt í bíó'}} />
				</Tab.Navigator>
			</NavigationContainer>
		</LinearGradient>
	);
}

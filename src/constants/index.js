// Ation types contstants
export const UPDATE_MOVIES = 'UPDATE_MOVIES';
export const UPDATE_UPCOMING_MOVIES = 'UPDATE_UPCOMING_MOVIES';
export const UPDATE_CINEMAS = 'UPDATE_CINEMAS';

export const APP_DONE_LOADING = 'APP_DONE_LOADING';
export const SPLASH_ANIMATION_DONE = 'SPLASH_ANIMATION_DONE';

export const REFRESH_CINEMA = 'REFRESH_CINEMA';
export const REFRESH_CINEMA_DETAILS = 'REFRESH_CINEMA_DETAILS';
export const REFRESH_MOVIE = 'REFRESH_MOVIE';
export const REFRESH_UPCOMING = 'REFRESH_UPCOMING';
export const REFRESH_UPCOMING_MOVIE = 'REFRESH_UPCOMING_MOVIE';

export const NEXT_VIDEO = 'NEXT_VIDEO';
export const PREV_VIDEO = 'PREV_VIDEO';
export const RESET_INDEX = 'RESET_INDEX';

// Navigation constants
export const CINEMAS = 'Cinemas';
export const CINEMA_DETAILS = 'CinemaDetails';
export const MOVIE_DETAILS = 'MovieDetails';
export const UPCOMING = 'Upcoming Movies';
export const UPCOMING_MOVIE_DETAILS = 'UpcomingDetails';

// A constant message for cinemas with no description
export const NO_CINEMA_DESCRIPTION = 'Engin lýsing.\n';

// A constant array to make dates look pretty
export const MONTH_ARRAY = [
	'error',
	'janúar',
	'febrúar',
	'mars',
	'apríl',
	'maí',
	'júní',
	'júlí',
	'ágúst',
	'september',
	'október',
	'nóvember',
	'desember',
];
